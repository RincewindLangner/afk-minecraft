# AFK Minecraft

The aim of the python program is to allow you to be AFK from Minecraft (java) to complete simple task 
such as fishing and Enderman farming; along with been able to place blocks at random from your 
hotbar.

## Prerequisites

* Designed for Minecraft Java

* Python 3.8

* Python Library
    * pynput 1.6.7
    * install via 
    `pip install pynput` or the link https://pypi.org/project/pynput/

## How to use

Once you have installed Python, run the afk-minecraft.py script. This should run in a terminal. 
Then go to Minecraft and execute the appropriated command listed below. 

### Do's and Don'ts

Once this program is running, using any of the the commands, expect the Stop action command,
will result in the program starting to preform that action. Even if you are not currently in the
Minecraft game window. It will run system wide. Make sure you only use the Key commands inside the game.

Unfortunately multiple commands can't be run at the same time. You will need to stop one command before
running the next. 

## Key commands

### Mob farming 

When you need to hit with your sward only every second to allow for the cool down.
* `<alt left> + y` 

### Random block placement

Select random blocks from your hotbar.
* `<alt left> + u`

### Constant left mouse press

Will keep your left mouse button pressed. Useful for mining or AFK fishing.
* `<alt left> + i`

### Feeding / off hand option

Use off hand every set amount of seconds. Default is 20 seconds between use.
Can be toggled on and off to rest.

Best used with food in off hand to help keep up stamina.  
#### Toggle on and off
* `<alt_l> + j`
#### Increase time
* `<alt_l> + h`
#### Decrease
* `<alt_l> + g`

### Stop action

Will reset all options back to default, including feeding to False 

* `<alt left> + <Backspace>`

## Functions

* Swing left mouse button every 1 second. 
* Random block placements from your Hotbar
* Hold left mouse button down for content action. 
* Add the option of 'use off hand' every 20 seconds for 2 seconds. Can be use
 to eat food to keep stamina up. 
* Can increase and decrease the off hand delay time between each use. 